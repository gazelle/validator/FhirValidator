package net.ihe.gazelle.fhir.validator.action;

import net.ihe.gazelle.fhir.validator.common.adapter.FhirResourceValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.adapter.FhirValidatorDescription;
import net.ihe.gazelle.validator.definition.adapter.FileTransformer;
import net.ihe.gazelle.preferences.GenericConfigurationManager;
import net.ihe.gazelle.validator.definition.adapter.auth.Pages;
import net.ihe.gazelle.validator.definition.adapter.webservice.GazelleFhirValidationDescriptionWS;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 * <p>FhirValidatorDescriptionDisplay class.</p>
 *
 * @author abe
 * @version 1.0: 09/04/18
 */
@ManagedBean(name = "fhirResourceValidatorDescriptionDisplay")
@ViewScoped
public class FhirResourceValidatorDescriptionDisplay extends FhirvalidatorDescriptionManager<FhirResourceValidatorDescription> {

    @Inject
    public void setFhirValidatorDescriptionTransactionManager(GazelleFhirValidationDescriptionWS fhirValidatorDescriptionWebService){
        this.fhirValidatorDescriptionWebService = fhirValidatorDescriptionWebService;
    }

    @Inject
    public void setApplicationConfigurationManager(GenericConfigurationManager genericConfigurationManager){
        this.genericConfigurationManager = genericConfigurationManager;
    }

    @PostConstruct
    public void initialize() {
        getValidatorDescriptionFromUrl();
    }

    public String editValidator() {
        return genericConfigurationManager.getApplicationUrl() + "/validators/editResourceValidator.seam?id=" + getValidatorDescription().getOid();
    }

    public String getStructureDefinitionContent() {
        if (getValidatorDescription().hasCustomStructureDefinition()) {
            return FileTransformer.convertXmlStructureDefinitionToHtml(getValidatorDescription().getSnapshotFilePath());
        } else {
            return null;
        }
    }

    @Override
    protected Class getEntityClass() {
        return FhirResourceValidatorDescription.class;
    }

    @Override
    public String editValidator(FhirValidatorDescription description) {
        return genericConfigurationManager.getApplicationUrl() + "/validators/editResourceValidator.seam?id=" + description.getOid();
    }

    @Override
    public String displayValidator(FhirValidatorDescription description) {
        return genericConfigurationManager.getApplicationUrl() + "/validators/displayResourceValidator.seam?id=" + description.getOid();
    }

    @Override
    public String getDisplayPage() {
        return Pages.DISPLAY_RESOURCE_VALIDATOR.getLink();
    }
}
