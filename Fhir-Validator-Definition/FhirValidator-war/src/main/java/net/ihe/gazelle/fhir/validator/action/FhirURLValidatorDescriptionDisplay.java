package net.ihe.gazelle.fhir.validator.action;


import net.ihe.gazelle.fhir.validator.common.adapter.FhirURLValidatorDescription;
import net.ihe.gazelle.preferences.GenericConfigurationManager;
import net.ihe.gazelle.validator.definition.adapter.auth.Pages;
import net.ihe.gazelle.validator.definition.adapter.webservice.GazelleFhirValidationDescriptionWS;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 * <p>FhirValidatorDescriptionDisplay class.</p>
 *
 * @author abe
 * @version 1.0: 09/04/18
 */
@ManagedBean(name = "fhirURLValidatorDescriptionDisplay")
@ViewScoped
public class FhirURLValidatorDescriptionDisplay extends FhirvalidatorDescriptionManager<FhirURLValidatorDescription> {

    @Inject
    public void setApplicationConfigurationManager(GenericConfigurationManager genericConfigurationManager){
        this.genericConfigurationManager = genericConfigurationManager;
    }

    @Inject
    public void setFhirValidatorDescriptionTransactionManager(GazelleFhirValidationDescriptionWS fhirValidatorDescriptionWebService){
        this.fhirValidatorDescriptionWebService = fhirValidatorDescriptionWebService;
    }

    @PostConstruct
    public void initialize() {
        getValidatorDescriptionFromUrl();
    }

    public String editValidator() {
        return genericConfigurationManager.getApplicationUrl() + "/validators/editURLValidator.seam?id=" + getValidatorDescription().getOid();
    }

    @Override
    public String getDisplayPage() {
        return Pages.DISPLAY_URL_VALIDATOR.getLink();
    }

    @Override
    protected Class getEntityClass() {
        return FhirURLValidatorDescription.class;
    }

}
