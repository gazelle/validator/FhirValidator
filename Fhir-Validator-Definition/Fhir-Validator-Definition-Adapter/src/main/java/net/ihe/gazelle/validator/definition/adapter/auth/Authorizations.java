package net.ihe.gazelle.validator.definition.adapter.auth;

import net.ihe.gazelle.cas.client.authentication.SSOIdentity;
import net.ihe.gazelle.pages.Authorization;

import javax.enterprise.inject.spi.CDI;

public enum Authorizations implements Authorization {

    ALL,
    LOGGED,
    ADMIN,
    MONITOR;

    @Override
    public boolean isGranted(Object... context) {
        switch (this) {
            case ALL:
                return true;
            case LOGGED:
                return CDI.current().select(SSOIdentity.class).get().isLoggedIn();
            case ADMIN:
                return CDI.current().select(SSOIdentity.class).get().hasRole("admin_role");
            case MONITOR:
                return CDI.current().select(SSOIdentity.class).get().hasRole("monitor_role");
            default:
                return false;
        }
    }

}
