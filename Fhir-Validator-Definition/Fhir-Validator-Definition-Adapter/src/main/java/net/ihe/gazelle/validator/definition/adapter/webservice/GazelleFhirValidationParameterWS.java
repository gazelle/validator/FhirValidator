/*
 * Copyright 2012 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.validator.definition.adapter.webservice;


import net.ihe.gazelle.validator.definition.adapter.dao.FhirRequestParameterDAOImpl;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.ext.Provider;
import java.io.Serializable;

/**
 * web service to act as a ValidatorDescription provider service
 */
@Provider
@Path(value = "/fhirValidatorParameter")
@Named("fhirValidatorParameterWebservice")
public class GazelleFhirValidationParameterWS implements Serializable {

    FhirRequestParameterDAOImpl dao;

    @Inject
    public void setFhirRequestParameterDAOImpl(FhirRequestParameterDAOImpl dao) {
        this.dao = dao;
    }


    /**
     * DELETE parameter
     *
     * @param databaseId the id of the parameter to delete
     */
    @DELETE
    @Deprecated
    // TODO: 27/11/2019  database id should not be exposed. Need to have another id in url param
    public void deleteParameter(String databaseId) {
        dao.deleteFhirRequestParameter(databaseId);
    }
}