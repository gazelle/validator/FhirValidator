package net.ihe.gazelle.fhir.validator.validation.application;

import ca.uhn.fhir.rest.api.EncodingEnum;
import net.ihe.gazelle.fhir.validator.common.business.FhirAssertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>ValidatorCommon class.</p>
 *
 * @author aberge
 * @version 1.0: 22/11/17
 */

public abstract class ValidatorCommon {

    protected String message;
    protected List<Notification> notifications;
    protected EncodingEnum format;
    protected int countErrors;
    protected int countWarnings;
    protected int countInfos;

    public ValidatorCommon(String message, EncodingEnum format){
        this.message = message;
        this.notifications = new ArrayList<Notification>();
        this.format = format;
        this.countErrors = 0;
        this.countWarnings = 0;
        this.countInfos = 0;
    }

    private String buildCompleteLocation(Integer countParameter, FhirAssertion assertion) {
        String location = assertion.getLocation();
        location = location.replace("$INDEX$", countParameter.toString());
        return location;
    }

    protected void fillNotification(Integer countParameter, FhirAssertion assertion, Notification notification) {
        if (countParameter != null) {
            String location = buildCompleteLocation(countParameter, assertion);
            notification.setLocation(location);
        } else {
            notification.setLocation(assertion.getLocation());
        }
        notification.setDescription(assertion.getDescription());
        notification.setIdentifiant(assertion.getIdentifier());
        notification.setAssertions(assertion.getTestedAssertion());
        if (notification.getClass().equals(Error.class)){
            countErrors++;
        } else if (notification.getClass().equals(Warning.class)){
            countWarnings++;
        } else if (notification.getClass().equals(Info.class)){
            countInfos++;
        }
    }

    protected MDAValidation buildReport() {
        MDAValidation validationResult = new MDAValidation();
        validationResult.getWarningOrErrorOrNote().addAll(notifications);
        ValidationCounters counters = new ValidationCounters();
        counters.setNrOfChecks(BigInteger.valueOf(notifications.size()));
        counters.setNrOfValidationErrors(BigInteger.valueOf(countErrors));
        counters.setNrOfValidationWarnings(BigInteger.valueOf(countWarnings));
        counters.setNrOfValidationInfos(BigInteger.valueOf(countInfos));
        validationResult.setValidationCounters(counters);
        String status = (countErrors > 0 ? ReportCreator.FAILED : ReportCreator.PASSED);
        validationResult.setResult(status);
        return validationResult;
    }
}
