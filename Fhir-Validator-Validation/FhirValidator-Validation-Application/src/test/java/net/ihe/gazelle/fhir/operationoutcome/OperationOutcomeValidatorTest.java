package net.ihe.gazelle.fhir.operationoutcome;

import ca.uhn.fhir.rest.api.EncodingEnum;

import net.ihe.gazelle.fhir.TestUtils;
import net.ihe.gazelle.fhir.validator.common.business.FhirValidatorDescription;
import net.ihe.gazelle.fhir.validator.validation.application.GazelleFhirValidator;
import net.ihe.gazelle.fhir.validator.validation.application.operationoutcome.OperationOutcomeAssertions;
import net.ihe.gazelle.fhir.validator.validation.application.operationoutcome.OperationOutcomeValidator;
import net.ihe.gazelle.validation.MDAValidation;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * <p>OperationOutcomeValidatorTest class.</p>
 *
 * @author aberge
 * @version 1.0: 24/11/17
 */

public class OperationOutcomeValidatorTest extends TestUtils {

    private static final Logger LOG = LoggerFactory.getLogger(OperationOutcomeValidatorTest.class);

    @Override
    protected MDAValidation validate(String message) {
        OperationOutcomeValidator validator = new OperationOutcomeValidator(message, EncodingEnum.XML);
        return validator.validateOperationOutcome();
    }


    @Test
    public void validateSeverityErrorKO(){
        String filepath = "src/test/resources/net.ihe.gazelle.fhir/operationoutcome/operationoutcome-003-ko.xml";
        try {
            Assert.assertTrue(validateNonValidFile(filepath, OperationOutcomeAssertions.CONSTRAINT_SEVERITY_ERROR, ERROR));
        }catch (IOException e){
            LOG.info(e.getMessage());
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void validateSeverityErrorOK(){
        String filepath = "src/test/resources/net.ihe.gazelle.fhir/operationoutcome/operationoutcome-ok.xml";
        try {
            Assert.assertTrue(validateValidFile(filepath, OperationOutcomeAssertions.CONSTRAINT_SEVERITY_ERROR));
        }catch (IOException e){
            LOG.info(e.getMessage());
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void validateRootKO(){
        String filepath = "src/test/resources/net.ihe.gazelle.fhir/operationoutcome/operationoutcome-002-ko.xml";
        try {
            Assert.assertTrue(validateNonValidFile(filepath, OperationOutcomeAssertions.CONSTRAINT_ROOT_ELEMENT, ERROR));
        }catch (IOException e){
            LOG.info(e.getMessage());
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void validateSeverityRootOK(){
        String filepath = "src/test/resources/net.ihe.gazelle.fhir/operationoutcome/operationoutcome-ok.xml";
        try {
            Assert.assertTrue(validateValidFile(filepath, OperationOutcomeAssertions.CONSTRAINT_ROOT_ELEMENT));
        }catch (IOException e){
            LOG.info(e.getMessage());
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void validateDiagnosticsPresentKO(){
        String filepath = "src/test/resources/net.ihe.gazelle.fhir/operationoutcome/operationoutcome-001-ko.xml";
        try {
            Assert.assertTrue(validateNonValidFile(filepath, OperationOutcomeAssertions.CONSTRAINT_DIAGNOSTICS, INFO));
        }catch (IOException e){
            LOG.info(e.getMessage());
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void validateDiagnosticsPresentOK(){
        String filepath = "src/test/resources/net.ihe.gazelle.fhir/operationoutcome/operationoutcome-ok.xml";
        try {
            Assert.assertTrue(validateValidFile(filepath, OperationOutcomeAssertions.CONSTRAINT_DIAGNOSTICS));
        }catch (IOException e){
            LOG.info(e.getMessage());
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void validateFullResponse() {
        String filepath = "src/test/resources/net.ihe.gazelle.fhir/operationoutcome/operationoutcome-ok.xml";
        isMDAValidationResultPassed(filepath);
    }

    @Test
    public void completeValidation() {
        String filepath = "src/test/resources/net.ihe.gazelle.fhir/operationoutcome/operationoutcome-xsd-ko.xml";
        try {
            String message = readFile(filepath);
            FhirValidatorDescription operationOutcomeXml = getOperationOutcomXml();
            GazelleFhirValidator validator = new GazelleFhirValidator(operationOutcomeXml);
            String result = validator.validateFhirMessage(message);
            LOG.info(result);
            System.out.println(result);
            Assert.assertNotNull(result);
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }

    protected void isMDAValidationResultPassed(String filepath) {
        try {
            String message = readFile(filepath);
            MDAValidation validation = validate(message);
            Assert.assertEquals("PASSED", validation.getResult());
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }


}
