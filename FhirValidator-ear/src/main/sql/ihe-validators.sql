
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, execute_schematron_validation, custom_structure_definition, execute_schema_validation, dtype, available)
VALUES (nextval('fhir_validator_description_id_seq'), '[ITI-83] Get Corresponding Identifiers', '1.3.6.1.4.1.12559.11.1.2.1.15.1', 'IHE',
        'PIXm', false, NULL , TRUE, 'url_val', FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, execute_schematron_validation, custom_structure_definition, execute_schema_validation, dtype, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-83] Parameters Resource XML', '1.3.6.1.4.1.12559.11.1.2.1.15.2', 'IHE',
        'PIXm', FALSE , NULL , TRUE, 'resource_val', FALSE);

INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, execute_schematron_validation, custom_structure_definition, execute_schema_validation, dtype, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-78] Query Patient Resource', '1.3.6.1.4.1.12559.11.1.2.1.15.4', 'IHE',
        'PDQm', FALSE , NULL , TRUE, 'url_val', FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, execute_schematron_validation, custom_structure_definition, execute_schema_validation, dtype, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-78] Query Patient Resource response XML', '1.3.6.1.4.1.12559.11.1.2.1.15.5', 'IHE',
        'PDQm', TRUE, NULL , TRUE, 'resource_val', FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, execute_schematron_validation, custom_structure_definition, execute_schema_validation, dtype, available)
VALUES (nextval('fhir_validator_description_id_seq'),'Operation Outcome XML', '1.3.6.1.4.1.12559.11.1.2.1.15.6', 'IHE', 'OperationOutcome', TRUE, NULL , TRUE, 'resource_val', FALSE);

INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, execute_schematron_validation, custom_structure_definition, execute_schema_validation, dtype, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-78] Retrieve Patient Resource', '1.3.6.1.4.1.12559.11.1.2.1.15.9', 'IHE',
        'PDQm_READ', FALSE, NULL , TRUE, 'url_val', FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, execute_schematron_validation, custom_structure_definition, execute_schema_validation, dtype, available)
VALUES (nextval('fhir_validator_description_id_seq'),'[ITI-78] Retrieve Patient Resource Response XML', '1.3.6.1.4.1.12559.11.1.2.1.15.10', 'IHE',
        'PDQm', TRUE, NULL , TRUE, 'resource_val', FALSE);
INSERT INTO fhir_validator_description(id, name, oid, descriminator, profile, execute_schematron_validation, custom_structure_definition, execute_schema_validation, dtype, available)
VALUES (nextval('fhir_validator_description_id_seq'),'FHIR Resource XML (DSTU3)', '1.3.6.1.4.1.12559.11.1.2.1.15.12', 'FHIR', 'FHIR', TRUE, NULL , TRUE, 'resource_val', FALSE);






INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[ITI-65] Provide Document Bundle Request XML', '1.3.6.1.4.1.12559.11.1.2.1.15.16', 'IHE',
                                                      'MHD', 'resource_val', FALSE,
                                                      '/opt/fhirvalidator/StructureDefinitionForTransactions/IHE.MHD.ITI-65.Request.xml', TRUE,
                                                      TRUE,
                                                      'http://ihe.net/fhir/tag/iti-65',
                                                      'http://hl7.org/fhir/StructureDefinition/Bundle', 20);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[ITI-65] Provide Document Bundle Request JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.17', 'IHE',
                                                      'MHD', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinitionForTransactions/IHE.MHD.ITI-65.Request.xml', TRUE,
                                                      TRUE,
                                                      'http://ihe.net/fhir/tag/iti-65',
                                                      'http://hl7.org/fhir/StructureDefinition/Bundle', 20);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[MHD] Document Manifest XML', '1.3.6.1.4.1.12559.11.1.2.1.15.18', 'IHE',
                                                      'MHD', 'resource_val', FALSE,
                                                      '/opt/fhirvalidator/StructureDefinition/IHE.MHD.DocumentManifest.xml', TRUE,
                                                      TRUE,
                                                      'http://ihe.net/fhir/StructureDefinition/IHE.MHD.DocumentManifest',
                                                      'http://hl7.org/fhir/StructureDefinition/DocumentManifest', 60);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[MHD] Document Manifest JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.19', 'IHE',
                                                      'MHD', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.MHD.DocumentManifest.xml', TRUE,
                                                      TRUE,
                                                      'http://ihe.net/fhir/StructureDefinition/IHE.MHD.DocumentManifest',
                                                      'http://hl7.org/fhir/StructureDefinition/DocumentManifest', 60);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[MHD] Document Reference (minimal for provide) XML', '1.3.6.1.4.1.12559.11.1.2.1.15.33', 'IHE',
                                                      'MHD', 'resource_val', FALSE,
                                                      '/opt/fhirvalidator/StructureDefinition/IHE.MHD.Provide.Minimal.DocumentReference.xml', TRUE,
                                                      TRUE,
                                                      'http://ihe.net/fhir/StructureDefinition/IHE.MHD.Provide.Minimal.DocumentReference',
                                                      'http://hl7.org/fhir/StructureDefinition/DocumentReference', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[MHD] Document Reference (minimal for provide) JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.32', 'IHE',
                                                      'MHD', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.MHD.Provide.Minimal.DocumentReference.xml', TRUE,
                                                      TRUE,
                                                      'http://ihe.net/fhir/StructureDefinition/IHE.MHD.Provide.Minimal.DocumentReference',
                                                      'http://hl7.org/fhir/StructureDefinition/DocumentReference', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[MHD] Provide Document Bundle (minimal) XML', '1.3.6.1.4.1.12559.11.1.2.1.15.42', 'IHE',
                                                      'MHD', 'resource_val', FALSE,
                                                      '/opt/fhirvalidator/StructureDefinition/IHE.MHD.ProvideDocumentBundle.Minimal.xml', TRUE,
                                                      TRUE,
                                                      'http://ihe.net/fhir/StructureDefinition/IHE.MHD.ProvideDocumentBundle.Minimal',
                                                      'http://hl7.org/fhir/StructureDefinition/Bundle', 40);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[MHD] Document Reference (minimal) JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.43', 'IHE',
                                                      'MHD', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.MHD.ProvideDocumentBundle.Minimal.xml', TRUE,
                                                      TRUE,
                                                      'http://ihe.net/fhir/StructureDefinition/IHE.MHD.ProvideDocumentBundle.Minimal',
                                                      'http://hl7.org/fhir/StructureDefinition/Bundle', 40);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[MHD] List XML', '1.3.6.1.4.1.12559.11.1.2.1.15.38', 'IHE',
                                                      'MHD', 'resource_val', FALSE,
                                                      '/opt/fhirvalidator/StructureDefinition/IHE.MHD.List.xml', TRUE,
                                                      TRUE,
                                                      'http://ihe.net/fhir/StructureDefinition/IHE.MHD.List',
                                                      'http://hl7.org/fhir/StructureDefinition/List', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[MHD] List JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.39', 'IHE',
                                                      'MHD', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.MHD.List.xml', TRUE,
                                                      TRUE,
                                                      'http://ihe.net/fhir/StructureDefinition/IHE.MHD.List',
                                                      'http://hl7.org/fhir/StructureDefinition/List', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[MHD] Document Reference (minimal for query) XML', '1.3.6.1.4.1.12559.11.1.2.1.15.40', 'IHE',
                                                      'MHD', 'resource_val', FALSE,
                                                      '/opt/fhirvalidator/StructureDefinition/IHE.MHD.Query.Minimal.DocumentReference.xml', TRUE,
                                                      TRUE,
                                                      'http://ihe.net/fhir/StructureDefinition/IHE.MHD.Query.Minimal.DocumentReference',
                                                      'http://hl7.org/fhir/StructureDefinition/DocumentReference', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[MHD] Document Reference (minimal for query) JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.41', 'IHE',
                                                      'MHD', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.MHD.Query.Minimal.DocumentReference.xml', TRUE,
                                                      TRUE,
                                                      'http://ihe.net/fhir/StructureDefinition/IHE.MHD.Query.Minimal.DocumentReference',
                                                      'http://hl7.org/fhir/StructureDefinition/DocumentReference', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[mCSD] Location Resource JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.20', 'IHE',
                                                      'mCSD', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.mCSD.Location.xml', TRUE, FALSE,
                                                      'http://ihe.net/fhir/StructureDefinition/IHE.mCSD.Location', 'http://hl7.org/fhir/StructureDefinition/Location', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[mCSD] Location Resource XML', '1.3.6.1.4.1.12559.11.1.2.1.15.21', 'IHE',
                                                      'mCSD', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.mCSD.Location.xml', TRUE, FALSE,
                                                      'http://ihe.net/fhir/StructureDefinition/IHE.mCSD.Location', 'http://hl7.org/fhir/StructureDefinition/Location', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[mCSD] Organization Resource JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.22', 'IHE',
                                                      'mCSD', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.mCSD.Organization.xml', TRUE,
                                                      FALSE, 'http://ihe.net/fhir/StructureDefinition/IHE.mCSD.Organization', 'http://hl7.org/fhir/StructureDefinition/Organization', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[mCSD] Organization Resource XML', '1.3.6.1.4.1.12559.11.1.2.1.15.23', 'IHE',
                                                      'mCSD', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.mCSD.Organization.xml', TRUE,
                                                      FALSE, 'http://ihe.net/fhir/StructureDefinition/IHE.mCSD.Organization', 'http://hl7.org/fhir/StructureDefinition/Organization', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[mCSD] Practitioner Resource JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.24', 'IHE',
                                                      'mCSD', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.mCSD.Practitioner.xml', TRUE,
                                                      FALSE, 'http://ihe.net/fhir/StructureDefinition/IHE.mCSD.Practitioner', 'http://hl7.org/fhir/StructureDefinition/Practitioner', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[mCSD] Practitioner Resource XML', '1.3.6.1.4.1.12559.11.1.2.1.15.25', 'IHE',
                                                      'mCSD', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.mCSD.Practitioner.xml', TRUE,
                                                      FALSE, 'http://ihe.net/fhir/StructureDefinition/IHE.mCSD.Practitioner', 'http://hl7.org/fhir/StructureDefinition/Practitioner', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[mCSD] Practitioner Role Resource JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.14', 'IHE',
                                                      'mCSD', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.mCSD.PractitionerRole.xml', TRUE,
                                                      FALSE, 'http://ihe.net/fhir/StructureDefinition/IHE.mCSD.PractitionerRole', 'http://hl7.org/fhir/StructureDefinition/PractitionerRole', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[mCSD] Practitioner Role Resource XML', '1.3.6.1.4.1.12559.11.1.2.1.15.15', 'IHE',
                                                      'mCSD', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.mCSD.PractitionerRole.xml', TRUE,
                                                      FALSE, 'http://ihe.net/fhir/StructureDefinition/IHE.mCSD.PractitionerRole', 'http://hl7.org/fhir/StructureDefinition/PractitionerRole', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[NPFSm] Document Reference XML', '1.3.6.1.4.1.12559.11.1.2.1.15.30', 'IHE',
                                                      'NPFSm', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.NPFSm.DocumentReference.xml', TRUE,
                                                      FALSE, 'http://www.ihe.net/fhir/StructureDefinition/IHE.NPFSm.DocumentReference', 'http://hl7.org/fhir/StructureDefinition/DocumentReference', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[NPFSm] Document Reference JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.31', 'IHE',
                                                      'NPFSm', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.NPFSm.DocumentReference.xml', TRUE,
                                                      FALSE, 'http://www.ihe.net/fhir/StructureDefinition/IHE.NPFSm.DocumentReference', 'http://hl7.org/fhir/StructureDefinition/DocumentReference', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[NPFSm] Submit File XML', '1.3.6.1.4.1.12559.11.1.2.1.15.28', 'IHE',
                                                      'NPFSm', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.NPFSm.SubmitFile.xml', TRUE,
                                                      FALSE, 'http://www.ihe.net/fhir/StructureDefinition/IHE.NPFSm.SubmitFile', 'http://hl7.org/fhir/StructureDefinition/Bundle', 80);
INSERT INTO fhir_validator_description (id, name, oid, descriminator, profile, dtype, execute_schematron_validation, custom_structure_definition, execute_schema_validation, available, structure_definition_id, base_definition, weight)
VALUES (nextval('fhir_validator_description_id_seq'), '[NPFSm] Submit File JSON', '1.3.6.1.4.1.12559.11.1.2.1.15.29', 'IHE',
                                                      'NPFSm', 'resource_val', FALSE, '/opt/fhirvalidator/StructureDefinition/IHE.NPFSm.SubmitFile.xml', TRUE,
                                                      FALSE, 'http://www.ihe.net/fhir/StructureDefinition/IHE.NPFSm.SubmitFile', 'http://hl7.org/fhir/StructureDefinition/Bundle', 80);