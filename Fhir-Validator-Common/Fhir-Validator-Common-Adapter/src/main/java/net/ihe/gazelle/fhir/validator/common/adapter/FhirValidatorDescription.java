package net.ihe.gazelle.fhir.validator.common.adapter;

import ca.uhn.fhir.rest.api.EncodingEnum;
import net.ihe.gazelle.fhir.validator.common.business.Exception.FhirValidatorException;
import net.ihe.gazelle.validator.validation.model.ValidatorDescription;


import javax.persistence.*;
import java.io.Serializable;

/**
 * <p>FhirValidatorDescription class.</p>
 *
 * @author abe
 * @version 1.0: 03/01/18
 */

@Entity
@Table(name = "fhir_validator_description", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "oid"))
@SequenceGenerator(name = "fhir_validator_description_sequence", sequenceName = "fhir_validator_description_id_seq", allocationSize = 1)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class FhirValidatorDescription implements ValidatorDescription, Serializable {

    private static final String COPY_SUFFIX = ".copy";

    @Id
    @GeneratedValue(generator = "fhir_validator_description_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "oid", unique = true, nullable = false)
    private String oid;

    @Column(name = "name")
    private String name;

    @Column(name = "descriminator")
    private String descriminator;

    @Column(name = "profile")
    private String profile;

    // encoding format will be detected by the validator at execution time
    @Transient
    private EncodingEnum format;



    @Column(name = "available")
    private boolean available;




    public FhirValidatorDescription() {

    }

    public FhirValidatorDescription(FhirValidatorDescription original){
        try {
            setOid(original.getOid());
        } catch (FhirValidatorException e) {
            this.oid = null;
        }
        this.name = original.getName() + COPY_SUFFIX;
        this.descriminator = original.getDescriminator();
        this.profile = original.getProfile();
        this.available = false;
    }

    /**
     * This constructor is used for unit testing
     * @param name
     * @param oid
     * @param descriminator
     * @param profile
     */
    public FhirValidatorDescription(String name, String oid, String descriminator, String profile) throws FhirValidatorException {
        this.name = name;
        setOid(oid);
        this.descriminator = descriminator;
        this.profile = profile;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public final void setOid(String oid) throws FhirValidatorException {
        if (oid == null || (oid.matches("^([0-2])((\\.0)|(\\.[1-9][0-9]*))*$"))) {
            this.oid = oid;
        } else {
            throw new FhirValidatorException();
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescriminator(String descriminator) {
        this.descriminator = descriminator;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public EncodingEnum getFormat() {
        return format;
    }

    public void setFormat(EncodingEnum format) {
        this.format = format;
    }

    @Override
    public String getOid() {
        return this.oid;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getDescriminator() {
        return this.descriminator;
    }

    // we are not dealing with soap messages, those methods are not used
    @Override
    public String getRootElement() {
        return null;
    }

    @Override
    public String getNamespaceURI() {
        return null;
    }

    @Override
    public boolean extractPartToValidate() {
        return false;
    }


    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }


}
