package net.ihe.gazelle.fhir.validator.common.peripherals.hapi;

import net.ihe.gazelle.fhir.validator.common.adapter.FhirResourceValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.business.Exception.FhirValidatorException;
import org.hl7.fhir.r4.model.StructureDefinition;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

/**
 * test Hapi dao
 */
public class HapiStructureDefLoaderAdapterDAOTest {

    /**
     * tested class
     */
    HapiAdapterDAO dao = new HapiAdapterDAO();

    /**
     * check that saveSnapshot creates a snapshotFile
     * @throws FhirValidatorException if build goes wrong
     */
    @Test
    public void saveSnapshotOnDisk() throws FhirValidatorException {
        //prepare
        FhirResourceValidatorDescription validatorName = new FhirResourceValidatorDescription();
        validatorName.setCustomStructureDefinition("src/test/resources/accountTest.xml");
        StructureDefinition definition = dao.getStructureDefinitionForValidator(validatorName);

        //execute
        dao.saveSnapshotOnDisk(definition, validatorName.getSnapshotFilePath());
        File snapshot = new File("src/test/resources/accountTest.snapshot.xml");

        //assert
        assertTrue(snapshot.exists());

        //clean
        snapshot.delete();

    }

    /**
     * test method build a structureDefinition from a correct StructureDef xml
     * @throws FhirValidatorException if build goes wrong
     */
    @Test
    public void buildStructureDefinitionFromFile() throws FhirValidatorException {
        StructureDefinition definition = dao.buildStructureDefinitionFromFile("src/test/resources/accountTest.xml");
        assertNotNull(definition);
    }

    /**
     * test method throw exception if wrongVersion
     * @throws FhirValidatorException if wrongVersion
     */
    @Test(expected = FhirValidatorException.class)
    public void testBuildStructureDefinitionFromFileWrongVersion() throws FhirValidatorException {
        dao.buildStructureDefinitionFromFile("src/test/resources/accountTestWrongVersion.xml");
    }

    /**
     * test method throw exception if path not found
     * @throws FhirValidatorException if path not found
     */
    @Test(expected = FhirValidatorException.class)
    public void testBuildStructureDefinitionFromFileWrongPath() throws FhirValidatorException {
        dao.buildStructureDefinitionFromFile("src/test/resources/thisFileDoesNotExist.xml");
    }

    /**
     * test get structureDefinitionfor validator works with correct validator
     * @throws FhirValidatorException if build goes wrong
     */
    @Test
    public void getStructureDefinitionForValidator() throws FhirValidatorException {
        FhirResourceValidatorDescription validatorName = new FhirResourceValidatorDescription();
        validatorName.setCustomStructureDefinition("src/test/resources/accountTest.xml");
        StructureDefinition definition = dao.getStructureDefinitionForValidator(validatorName);
        assertNotNull(definition);
    }

    /**
     * test get structureDefinitionforvalidator returns null if no customStrucure def is defined
     * @throws FhirValidatorException if build goes wrong
     */
    @Test
    public void getStructureDefinitionForValidatorNoCustomStructureDef() throws FhirValidatorException {
        FhirResourceValidatorDescription validatorName = new FhirResourceValidatorDescription();
        StructureDefinition definition = dao.getStructureDefinitionForValidator(validatorName);
        assertNull(definition);
    }
}
