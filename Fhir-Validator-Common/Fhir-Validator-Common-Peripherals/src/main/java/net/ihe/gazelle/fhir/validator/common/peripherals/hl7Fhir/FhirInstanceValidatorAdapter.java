package net.ihe.gazelle.fhir.validator.common.peripherals.hl7Fhir;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.context.support.IValidationSupport;

public class FhirInstanceValidatorAdapter extends org.hl7.fhir.common.hapi.validation.validator.FhirInstanceValidator {

    public FhirInstanceValidatorAdapter(FhirContext theContext) {
        super(theContext);
    }

    public FhirInstanceValidatorAdapter(IValidationSupport theValidationSupport) {
        super(theValidationSupport);
    }
}
