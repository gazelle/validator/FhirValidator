package net.ihe.gazelle.fhir.validator.common.peripherals.hapi;

import ca.uhn.fhir.parser.IParser;
import net.ihe.gazelle.fhir.constants.FhirParserProvider;
import net.ihe.gazelle.fhir.validator.common.adapter.FhirResourceValidatorDescription;
import net.ihe.gazelle.fhir.validator.common.business.Exception.FhirValidatorException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.StructureDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Hapi adapter to deal with DAO
 */
public class HapiAdapterDAO {
    private static final Logger LOG = LoggerFactory.getLogger(HapiAdapterDAO.class);

    /**
     * access disk to save structureDefinition snapshot
     * @param structureDefinition resource to save
     * @param snapFilePath to get the path
     */
    public void saveSnapshotOnDisk(StructureDefinition structureDefinition, String snapFilePath) {
        IParser parser = FhirParserProvider.getXmlParser();
        try (FileWriter writer = new FileWriter(snapFilePath)) {
            parser.encodeResourceToWriter(structureDefinition, writer);
        } catch (IOException e) {
            LOG.warn(e.getMessage());
        }
    }

    /**
     * Load structure def from file
     *
     * @param filepath path of the resource
     * @return the laoded structure def
     * @throws FhirValidatorException if load failed
     */
    public StructureDefinition buildStructureDefinitionFromFile(String filepath) throws FhirValidatorException {
        FhirValidatorException exception = null;
        StructureDefinition definition = null;
        try (InputStreamReader reader = new InputStreamReader(new FileInputStream(filepath), StandardCharsets.UTF_8)) {
            IBaseResource resource = HapiParserAdapter.parseXMLBaseResource(reader);
            if(resource instanceof StructureDefinition) {
                definition = (StructureDefinition) resource;
            } else {
                LOG.warn("This file is not a StructureDefinition: " + filepath);
                exception = new FhirValidatorException("This file is not a StructureDefinition: " + filepath +
                        " The found class is: " + resource.getClass().getName());
            }
        } catch (FileNotFoundException e) {
            LOG.warn("Cannot read file " + filepath);
            exception = new FhirValidatorException("Cannot read file " + filepath, e);
        } catch (Exception e){
            exception = new FhirValidatorException(e.getMessage(), e);
        }
        if (exception != null){
            throw exception;
        } else {
            return definition;
        }
    }

    /**
     * load structure def from validator
     * @param validatorName the validator name
     * @return the laoded structure def
     * @throws FhirValidatorException if load failed
     */
    public StructureDefinition getStructureDefinitionForValidator(FhirResourceValidatorDescription validatorName) throws FhirValidatorException {
        if (validatorName != null
                && validatorName.getCustomStructureDefinition() != null
                && !validatorName.getCustomStructureDefinition().isEmpty()) {
            return buildStructureDefinitionFromFile(validatorName.getCustomStructureDefinition());
        } else {
            return null;
        }
    }

}
