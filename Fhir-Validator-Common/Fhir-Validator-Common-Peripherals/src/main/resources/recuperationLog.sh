#!/bin/sh
echo "Enter Date to look for in format YYYY-MM-DD HH:mm (you can specify only what you need)"
read DATE
if [ -z "$DATE" ]; then echo "No date given, exiting"; exit; fi
grep -rn /usr/local/wildfly10/standalone/log/ -e "$DATE.*---->" > fhir.log
